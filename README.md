* Quick summary

  This project is for MOST project in NTUST, the goal of this program is
  that you can instruct the crawler to obtain apks for you via a wish list(.txt file),
  and after the apk is downloaded, some specific information will be
  extracted out and save into the database.

* Summary of set up

  This project is build upon Spring Boot, in general, just install the 
  database(default is MySQL) and create table(see schema.sql), 
  configure jdbc.properties, and re-compile the project to generate 
  the executable jar file, then you can directly launch the project.

* Dependencies

  The crawler API was downloaded from here:
  https://github.com/Akdeniz/google-play-crawler

  For other dependencied, please refer to pom.xml.