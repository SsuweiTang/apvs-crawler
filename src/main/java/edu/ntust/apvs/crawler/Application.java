package edu.ntust.apvs.crawler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import edu.ntust.apvs.crawler.task.CrawlerScheduledTask;

/**
 * @author Carl Adler (C.A.)
 * */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {
	
	public static void main(String[] args) {
		SpringApplication.run(CrawlerScheduledTask.class, args);
	}
	
}
