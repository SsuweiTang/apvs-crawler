package edu.ntust.apvs.crawler.util.fingerprint;

import edu.ntust.apvs.crawler.domain.APKSubFileFingerPrint;

public class ObjectTransformer {
	
	public static APKSubFileFingerPrint transformSubCategoryOfResToListAPKSubFileFingerPrint(String ResCategory, String ResSubCategoryFingerPrint) {
		APKSubFileFingerPrint FingerResSubCategory = new APKSubFileFingerPrint();
		FingerResSubCategory.setFileName(ResCategory);
		FingerResSubCategory.setSha1Hex(ResSubCategoryFingerPrint);
		return FingerResSubCategory;
	}

}
