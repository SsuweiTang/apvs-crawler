package edu.ntust.apvs.crawler.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ntust.apvs.crawler.domain.APKDownloadInfo;
import edu.ntust.apvs.crawler.domain.APKManifest;

/**
 * @author Carl Adler (C.A.)
 * */
public class APKFileHandler {

	private static Logger logger = LoggerFactory
			.getLogger(APKFileHandler.class);

	public static List<String> getListFromFile(String listType) {
		List<String> customList = new LinkedList<String>();
		try (Stream<String> lines = Files.lines(new File(listType).toPath(),
				StandardCharsets.UTF_8)) {
			if (listType.equals(APVSConstantDescriptor.LIST_FOR_CRAWL))
				lines.filter(s -> !s.isEmpty()).forEach(s -> customList.add(s));
			lines.close();
		} catch (IOException e) {
			logger.error("Error when get list from file {}...", listType);
		}
		return customList;
	}

	public static APKManifest extractMetaInfo(APKDownloadInfo downloadInfo,
			String apkFileName) {
		logger.debug("Extracting meta info from apk: {} ...", apkFileName);
		APKManifest apkManifest = null;

		try {
			JarFile apkSourceFile = new JarFile(
					APVSConstantDescriptor.APK_FILE_LOCATION + apkFileName);

			Manifest manifest = apkSourceFile.getManifest();
			Map<String, Attributes> entryMap = manifest.getEntries();
			apkManifest = new APKManifest();
			apkManifest.setApkFileName(apkFileName);
			apkManifest.setShaForAndroidManifestXml(entryMap.get(
					APVSConstantDescriptor.ANDROID_MANIFEST_XML).getValue(
					APVSConstantDescriptor.TARGET_KEY));
			apkManifest.setShaForResourcesArsc(entryMap.get(
					APVSConstantDescriptor.RESOURCES_ARSC).getValue(
					APVSConstantDescriptor.TARGET_KEY));
			apkManifest.setShaForClassesDex(entryMap.get(
					APVSConstantDescriptor.CLASSES_DEX).getValue(
					APVSConstantDescriptor.TARGET_KEY));
			apkManifest.setMnftEntrySize(entryMap.size());
			apkManifest.setVersionCode(String.valueOf(downloadInfo
					.getVersionCode()));
			apkSourceFile.close();
		} catch (IOException e) {
			logger.error("Error when extracting APK meta info: {}", e);
		}
		// logger.debug(apkManifest.toString());
		logger.debug("Extract process complete.");
		return apkManifest;
	}

	public static void showAPKInfo(String apkFileName) {
		logger.debug("Extracting meta info from apk: {} ...", apkFileName);
		try {
			JarFile apkSourceFile = new JarFile(
					APVSConstantDescriptor.APK_FILE_LOCATION + apkFileName);
			Manifest manifest = apkSourceFile.getManifest();
			Map<String, Attributes> entryMap = manifest.getEntries();
			logger.debug("ApkFileName = {}", apkFileName);
			logger.debug("ShaForAndroidManifestXml = {}",
					entryMap.get(APVSConstantDescriptor.ANDROID_MANIFEST_XML)
							.getValue(APVSConstantDescriptor.TARGET_KEY));
			logger.debug("ShaForResourcesArsc = {}",
					entryMap.get(APVSConstantDescriptor.RESOURCES_ARSC)
							.getValue(APVSConstantDescriptor.TARGET_KEY));
			logger.debug(
					"ShaForClassesDex = {}",
					entryMap.get(APVSConstantDescriptor.CLASSES_DEX).getValue(
							APVSConstantDescriptor.TARGET_KEY));
			logger.debug("MnftEntrySize = {}", entryMap.size());
			apkSourceFile.close();
		} catch (Exception e) {
			logger.error("Error when extracting APK meta info: {}", e);
		}
	}

}
