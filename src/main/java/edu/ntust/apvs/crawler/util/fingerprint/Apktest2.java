package edu.ntust.apvs.crawler.util.fingerprint;


import java.io.File;

import edu.ntust.apvs.crawler.domain.APKFingerPrintInfo;
import edu.ntust.apvs.crawler.service.impl.APKFingerPrintServiceImpl;
import edu.ntust.apvs.crawler.util.APVSConstantDescriptor;

public class Apktest2 {

	public static void main(String[] args){
		APKFingerPrintServiceImpl  apkfingerprintservice=new  APKFingerPrintServiceImpl();
		APKFingerPrintInfo apkfingerprint=apkfingerprintservice.generateAPKFingerPrintFromAPKFile("com.tencent.mm.apk");
		
		
		System.out.println("f_classes  " + apkfingerprint.getFingerPrintForClassesDex());
		System.out.println("f_resources  "
				+ apkfingerprint.getFingerPrintForResourcesArsc());
		System.out.println("f_AndroidManifest  "
				+ apkfingerprint.getFingerPrintForAndroidManifestXml());

		System.out.println("f_.xml  " + apkfingerprint.getFingerPrintForResXml());
		System.out.println("f_.png  " + apkfingerprint.getFingerPrintForResImg());

		System.out.println("f_res   " + apkfingerprint.getFingerPrintForRes());

		System.out.println("------------------------------------------");

		System.out.println("f_all  " + apkfingerprint.getFingerPrintForEntireAPKFile());

	}
	
	
	private static boolean deleteDirectoryForCutFile(File fileFolder) {

		 File[] files = fileFolder.listFiles();
		    if(files!=null) { 
		        for(File f: files) {
		            if(f.isDirectory()) {
		            	deleteDirectoryForCutFile(f);
		            } else if(f.isFile()) {
		                f.delete();
		            }
		        }
		    }
		return fileFolder.delete();
	}

	

	


}
