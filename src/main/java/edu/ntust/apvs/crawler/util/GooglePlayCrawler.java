package edu.ntust.apvs.crawler.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.akdeniz.googleplaycrawler.GooglePlay.AppDetails;
import com.akdeniz.googleplaycrawler.GooglePlay.DetailsResponse;
import com.akdeniz.googleplaycrawler.GooglePlay.Offer;
import com.akdeniz.googleplaycrawler.GooglePlayAPI;
import com.akdeniz.googleplaycrawler.Utils;

import edu.ntust.apvs.crawler.domain.APKDownloadInfo;

/**
 * @author Carl Adler (C.A.)
 * */
public class GooglePlayCrawler {
	
	private static Logger logger = LoggerFactory.getLogger(GooglePlayCrawler.class);
	
	private static GooglePlayAPI service;

	private static HttpClient getProxiedHttpClient(String host, Integer port) throws Exception {
		HttpClient client = new DefaultHttpClient(GooglePlayAPI.getConnectionManager());
		client.getConnectionManager().getSchemeRegistry().register(Utils.getMockedScheme());
		HttpHost proxy = new HttpHost(host, port);
		client.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
		return client;
	}
	
	public void setup() throws Exception {
		logger.debug("Crawler setup start.");
		
		Properties properties = new Properties();
		properties.load(new FileInputStream(APVSConstantDescriptor.CRAWLER_HOME + "login.conf"));

		String email = properties.getProperty("email");
		String password = properties.getProperty("password");

		String host = properties.getProperty("host");
		String port = properties.getProperty("port");

		service = new GooglePlayAPI(email, password);

		if (host != null && port != null) {
			service.setClient(getProxiedHttpClient(host, Integer.valueOf(port)));
		}
		logger.debug("Crawler setup complete.");
	}
	
	public void checkin() throws Exception {
		service.checkin();
		logger.debug("Crawler checkin complete.");
	}
	
	public void login() throws Exception {
		logger.debug("Allow server to catch up after checkin...");
		Thread.sleep(5000);
		logger.debug("Start login...");
		service.login();
		logger.debug("Login successfully...");
	}
	
	public void uploadDeviceConfiguration() throws Exception {
		logger.debug("Uploading device config...");
		service.uploadDeviceConfig();
		logger.debug("Uploade device config complete.");
	}
	
	public APKDownloadInfo getDownloadInfo(final String packageName) throws Exception {
		logger.debug("Preprocessing before download apk: {}.apk", packageName);
		APKDownloadInfo downloadInfo = null;
		DetailsResponse details = service.details(packageName);
		AppDetails appDetails = details.getDocV2().getDetails().getAppDetails();
		Offer offer = details.getDocV2().getOffer(0);
		
		int versionCode = appDetails.getVersionCode();
		int offerType = offer.getOfferType();
		boolean checkoutRequired = offer.getCheckoutFlowRequired();
		// paid application...ignore
		if (checkoutRequired) {
			logger.debug("This is a paid application, ignore it.");
			return null;
		} else {
			downloadInfo = new APKDownloadInfo();
			downloadInfo.setPackageName(packageName);
			downloadInfo.setVersionCode(versionCode);
			downloadInfo.setOfferType(offerType);
		}
		return downloadInfo;
	}
	
	public void downloadAPKFileWithAPKDownloadInfo(final APKDownloadInfo downloadInfo) throws Exception {
		logger.debug("Start downloading...");
		String packageName = downloadInfo.getPackageName();
		InputStream downloadStream = service.download(packageName, downloadInfo.getVersionCode(), downloadInfo.getOfferType());
		FileOutputStream outputStreamForRealFileEntity = new FileOutputStream(new File(APVSConstantDescriptor.APK_FILE_LOCATION + packageName + ".apk"));
		byte buffer[] = new byte[1024];
		for (int k = 0; (k = downloadStream.read(buffer)) != -1;) {
			outputStreamForRealFileEntity.write(buffer, 0, k);
		}
		downloadStream.close();
		outputStreamForRealFileEntity.close();
		logger.debug("Download successfully...");
	}

}
