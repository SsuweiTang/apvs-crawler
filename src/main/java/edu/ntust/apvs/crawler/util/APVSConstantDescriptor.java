package edu.ntust.apvs.crawler.util;

/**
 * @author Carl Adler (C.A.)
 * */
public class APVSConstantDescriptor {
	
	public static final String CRAWLER_HOME = "D:\\GooglePlayCrawler_Home\\";
	public static final String LIST_LOCATION = "D:\\GooglePlayCrawler_Home\\list\\";
	public static final String APK_FILE_LOCATION = "D:\\GooglePlayCrawler_Home\\CrawlerSample\\";
	public static final String LIST_FOR_CRAWL = LIST_LOCATION + "crawl_list.txt";
	public static final String TARGET_KEY = "SHA1-Digest";
	public static final String ANDROID_MANIFEST_XML = "AndroidManifest.xml";
	public static final String RESOURCES_ARSC = "resources.arsc";
	public static final String CLASSES_DEX = "classes.dex";
	
	public static final String APK_FILE_LOCATION2="D:\\apk\\";
	public static final String APK_FILE_NAME="M10209101_HW4.apk";
	
	public static final String RES_CATEGORY_PNG=".png";
	public static final String RES_CATEGORY_XML=".xml";
	public static final String RES_CATEGORY_JPG=".jpg";
	public static final String CERT_SF="META-INF/CERT.SF";
	public static final String CERT_RSA="META-INF/CERT.RSA";
	
	private final static int DOWNLOAD_STRATEGY_FOR_EXIST_APK = 0;
	
	
}
