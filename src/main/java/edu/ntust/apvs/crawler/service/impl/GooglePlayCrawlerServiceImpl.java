package edu.ntust.apvs.crawler.service.impl;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import edu.ntust.apvs.crawler.dao.CrawlerTaskDao;
import edu.ntust.apvs.crawler.domain.APKDownloadInfo;
import edu.ntust.apvs.crawler.domain.APKFingerPrintInfo;
import edu.ntust.apvs.crawler.domain.APKManifest;
import edu.ntust.apvs.crawler.service.APKFingerPrintService;
import edu.ntust.apvs.crawler.service.GooglePlayCrawlerService;
import edu.ntust.apvs.crawler.util.APKFileHandler;
import edu.ntust.apvs.crawler.util.APVSConstantDescriptor;
import edu.ntust.apvs.crawler.util.GooglePlayCrawler;

/**
 * @author Carl Adler (C.A.)
 * */
public class GooglePlayCrawlerServiceImpl implements GooglePlayCrawlerService {

	private static final Logger logger = LoggerFactory.getLogger(GooglePlayCrawlerServiceImpl.class);
	
	private final static int DOWNLOAD_STRATEGY_FOR_DEFAULT = 0;
	private final static int DOWNLOAD_STRATEGY_FOR_NEW_APK = 1;
	private final static int DOWNLOAD_STRATEGY_FOR_EXIST_APK = 2;

	private CrawlerTaskDao crawlerTaskDao;
	private GooglePlayCrawler crawler;
	private APKFingerPrintService apkfingerprintservice;

	public void setApkfingerprintservice(APKFingerPrintService apkfingerprintservice) {
		this.apkfingerprintservice = apkfingerprintservice;
	}

	public void setCrawlerTaskDao(CrawlerTaskDao crawlerTaskDao) {
		this.crawlerTaskDao = crawlerTaskDao;
	}

	public void setCrawler(GooglePlayCrawler crawler) {
		this.crawler = crawler;
	}

	@Override
	public void crawlAPKFilesWithCustomList() {
		List<String> listForCrawl = APKFileHandler.getListFromFile(APVSConstantDescriptor.LIST_FOR_CRAWL);
		crawlerInitialization();
		listForCrawl.forEach(packageName -> downloadAPKFile(packageName));
	}

	private void crawlerInitialization() {
		try {
			crawler.setup();
			crawler.checkin();
			crawler.login();
			crawler.uploadDeviceConfiguration();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	
	@Transactional
	private void downloadAPKFile(String packageName) {
		try {
			APKDownloadInfo nonPaidAPKInfo = crawler.getDownloadInfo(packageName);
			if(nonPaidAPKInfo != null) {
				String apkFileName = packageName + ".apk";
				int downloadStrategy = checkDownloadStrategyOfAPK(apkFileName, nonPaidAPKInfo.getVersionCode());
				if(downloadStrategy > 0) {
					crawler.downloadAPKFileWithAPKDownloadInfo(nonPaidAPKInfo);
					APKManifest manifest = APKFileHandler.extractMetaInfo(nonPaidAPKInfo, apkFileName);
					APKFingerPrintInfo apkfingerprint= apkfingerprintservice.generateAPKFingerPrintFromAPKFile(apkFileName);
					switch (downloadStrategy) {
					case DOWNLOAD_STRATEGY_FOR_NEW_APK:
						crawlerTaskDao.saveAPKMetaInfo(manifest);
						crawlerTaskDao.saveAPKFingerPrintInfo(apkfingerprint);
						break;
					case DOWNLOAD_STRATEGY_FOR_EXIST_APK:
						manifest.setId(crawlerTaskDao.queryIdByAPKFileName(apkFileName));
						crawlerTaskDao.updateAPKMetaInfo(manifest);
						crawlerTaskDao.updateAPKFingerPrintInfo(apkfingerprint);
						break;
					default:
						break;
					}
					logger.debug("Delete file result: {}", deleteDownloadFile(apkFileName));
				}
			}
		} catch (Exception e) {
			logger.error("Oops, something wrong when downloading apk: {}.apk", packageName);
			logger.error(e.getMessage());
		}
	}
	
	private int checkDownloadStrategyOfAPK(String apkFileName, int newVersionCode) {
		int downloadStrategy = DOWNLOAD_STRATEGY_FOR_DEFAULT;
		if (!crawlerTaskDao.findByAPKFileName(apkFileName).isEmpty()) {
			if (crawlerTaskDao.checkVersionCodeEquality(apkFileName, String.valueOf(newVersionCode))) {
				logger.debug("No new release version, stop downloading...");
			} else {
				logger.debug("The APK has new version, start downloading...");
				downloadStrategy = DOWNLOAD_STRATEGY_FOR_EXIST_APK;
			}
		} else {
			logger.debug("The APK hasn't been in sample database.");
			downloadStrategy = DOWNLOAD_STRATEGY_FOR_NEW_APK;
		}
		return downloadStrategy;
	}
	
	private boolean deleteDownloadFile(String apkFileName) {
		logger.debug("Delete file: {}", apkFileName);
		File deletedFile = new File(APVSConstantDescriptor.APK_FILE_LOCATION + apkFileName);
		return deletedFile.delete();
	}
	
	
}
