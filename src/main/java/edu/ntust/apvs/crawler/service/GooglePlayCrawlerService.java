package edu.ntust.apvs.crawler.service;

/**
 * @author Carl Adler (C.A.)
 * */
public interface GooglePlayCrawlerService {
	
	public void crawlAPKFilesWithCustomList();
	
}
