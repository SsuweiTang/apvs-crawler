package edu.ntust.apvs.crawler.service;

import edu.ntust.apvs.crawler.domain.APKFingerPrintInfo;

public interface APKFingerPrintService {
	
	public APKFingerPrintInfo generateAPKFingerPrintFromAPKFile(String apkFile);

}
