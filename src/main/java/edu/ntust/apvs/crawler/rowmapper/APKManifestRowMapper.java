package edu.ntust.apvs.crawler.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import edu.ntust.apvs.crawler.domain.APKManifest;

/**
 * @author Carl Adler (C.A.)
 * */
public final class APKManifestRowMapper implements RowMapper<APKManifest>{

	@Override
	public APKManifest mapRow(ResultSet rs, int rowNum) throws SQLException {
		APKManifest apkManifest = new APKManifest();
		apkManifest.setId(rs.getLong("id"));
		apkManifest.setApkFileName(rs.getString("apk_file_name"));
		apkManifest.setShaForAndroidManifestXml(rs.getString("sha_andro_mnft_xml"));
		apkManifest.setShaForResourcesArsc(rs.getString("sha_res_arsc"));
		apkManifest.setShaForClassesDex(rs.getString("sha_cls_dex"));
		apkManifest.setMnftEntrySize(rs.getInt("mnft_entry_size"));
		apkManifest.setVersionCode(rs.getString("version_code"));
		return apkManifest;
	}

}
