package edu.ntust.apvs.crawler.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import edu.ntust.apvs.crawler.service.GooglePlayCrawlerService;

/**
 * @author Carl Adler (C.A.)
 * */
@EnableScheduling
@ImportResource("app-context.xml")
public class CrawlerScheduledTask {
	
	private static final Logger logger = LoggerFactory.getLogger(CrawlerScheduledTask.class);
    
	@Autowired
	private GooglePlayCrawlerService crawlerService;

	@Scheduled(fixedRate = 86400000)
    public void crawlAPKFileForCustomProperty() {
		long startTime = 0;
		long endTime = 0;
		logger.debug("----- Task for crawl APK files start -----");
		startTime = System.currentTimeMillis();
    	crawlerService.crawlAPKFilesWithCustomList();
    	endTime = System.currentTimeMillis();
    	logger.debug("----- Task for crawl APK files complete... -----");
    	logger.debug("Task took time: {} mins", (endTime - startTime)/60000);
    }
	
}
