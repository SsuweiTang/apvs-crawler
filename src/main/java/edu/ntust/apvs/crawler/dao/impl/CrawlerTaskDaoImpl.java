package edu.ntust.apvs.crawler.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import edu.ntust.apvs.crawler.dao.CrawlerTaskDao;
import edu.ntust.apvs.crawler.domain.APKFingerPrintInfo;
import edu.ntust.apvs.crawler.domain.APKManifest;
import edu.ntust.apvs.crawler.rowmapper.APKManifestRowMapper;

/**
 * @author Carl Adler (C.A.)
 * */
@Transactional
public class CrawlerTaskDaoImpl implements CrawlerTaskDao, InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(CrawlerTaskDaoImpl.class);

	private static final String SQL_FOR_QUERY_APK_ID = "select id from apk_info where apk_file_name = ?;";
	private static final String SQL_FOR_CHECK_EXISTENCE_OF_APK_META_INFO = "select * from apk_info where apk_file_name = ?;";
	private static final String SQL_FOR_CHECK_VERSION_CODE_EQUALITY_FOR_APK = "select version_code from apk_info where apk_file_name = ?;";
	private static final String SQL_FOR_INSERT_APK_INFO = "INSERT INTO "
			+ "apk_info(apk_file_name, sha_andro_mnft_xml, sha_res_arsc, sha_cls_dex, mnft_entry_size, version_code) VALUES(?, ?, ?, ?, ?, ?);";
	
	private static final String SQL_FOR_INSERT_APK_FP_INFO = "INSERT INTO "
			+ "apk_fp_info(fp_entire_apk, fp_android_mnft, fp_res_arsc, fp_class_dex, fp_res, fp_res_xml,fp_res_img) VALUES(?, ?, ?, ?, ?, ?,?);";
	
	private static final String SQL_FOR_UPDATE_APK_FP_INFO = "UPDATE apk_fp_info SET fp_entire_apk = ?,"
			+ "fp_android_mnft = ?,fp_android_mnft = ?,fp_res_arsc = ?,fp_class_dex = ?,fp_res_xml = ?,fp_res_img = ? WHERE id = ?;";
	
	
	private static final String SQL_FOR_UPDATE_APK_INFO = "UPDATE apk_info SET apk_file_name = ?,"
			+ "sha_andro_mnft_xml = ?,sha_res_arsc = ?,sha_cls_dex = ?,mnft_entry_size = ?,version_code = ? WHERE id = ?;";
	
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (dataSource == null)
			throw new BeanCreationException(
					"Must set dataSource for crawlerTaskDao...");
		else
			logger.debug("Initialize crawlerTaskDao successfully...");
	}
	
	@Override
	@Transactional(readOnly = true)
	public long queryIdByAPKFileName(String apkFileName) {
		return jdbcTemplate.queryForObject(SQL_FOR_QUERY_APK_ID, new Object[] {apkFileName}, Long.class);
	}

	@Override
	@Transactional(readOnly = true)
	public List<APKManifest> findByAPKFileName(String apkFileName) {
		return jdbcTemplate.query(
				SQL_FOR_CHECK_EXISTENCE_OF_APK_META_INFO,
				new APKManifestRowMapper(), new Object[] { apkFileName });
	}

	@Override
	@Transactional(readOnly = true)
	public boolean checkVersionCodeEquality(String apkFileName, String versionCode) {
		return (versionCode.equals(jdbcTemplate.queryForObject(
				SQL_FOR_CHECK_VERSION_CODE_EQUALITY_FOR_APK,
				new Object[] { apkFileName }, String.class)));
	}

	@Override
	public int saveAPKMetaInfo(APKManifest apkManifest) {
		return jdbcTemplate.update(
				SQL_FOR_INSERT_APK_INFO,
				new Object[] { apkManifest.getApkFileName(),
						apkManifest.getShaForAndroidManifestXml(),
						apkManifest.getShaForResourcesArsc(),
						apkManifest.getShaForClassesDex(),
						apkManifest.getMnftEntrySize(),
						apkManifest.getVersionCode()});	}

	@Override
	public boolean updateAPKMetaInfo(APKManifest apkManifest) {
		return jdbcTemplate.update(
				SQL_FOR_UPDATE_APK_INFO,
				new Object[] { apkManifest.getApkFileName(),
						apkManifest.getShaForAndroidManifestXml(),
						apkManifest.getShaForResourcesArsc(),
						apkManifest.getShaForClassesDex(),
						apkManifest.getMnftEntrySize(),
						apkManifest.getVersionCode(),
						apkManifest.getId()}) > 0;
	}

	@Override
	public int saveAPKFingerPrintInfo(APKFingerPrintInfo apkfingerprint) {
		return jdbcTemplate.update(
				SQL_FOR_INSERT_APK_FP_INFO,
				new Object[] { apkfingerprint.getFingerPrintForEntireAPKFile(),
						apkfingerprint.getFingerPrintForAndroidManifestXml(),
						apkfingerprint.getFingerPrintForResourcesArsc(),
						apkfingerprint.getFingerPrintForClassesDex(),
						apkfingerprint.getFingerPrintForRes(),
						apkfingerprint.getFingerPrintForResXml(),
						apkfingerprint.getFingerPrintForResImg()});
		
	}

	@Override
	public boolean updateAPKFingerPrintInfo(APKFingerPrintInfo apkfingerprint) {
		return jdbcTemplate.update(
				SQL_FOR_UPDATE_APK_FP_INFO,
				new Object[] {  apkfingerprint.getFingerPrintForEntireAPKFile(),
						apkfingerprint.getFingerPrintForAndroidManifestXml(),
						apkfingerprint.getFingerPrintForResourcesArsc(),
						apkfingerprint.getFingerPrintForClassesDex(),
						apkfingerprint.getFingerPrintForRes(),
						apkfingerprint.getFingerPrintForResXml(),
						apkfingerprint.getFingerPrintForResImg(),
						apkfingerprint.getId()}) > 0;
	}

}
