package edu.ntust.apvs.crawler.dao;

import java.util.List;

import edu.ntust.apvs.crawler.domain.APKFingerPrintInfo;
import edu.ntust.apvs.crawler.domain.APKManifest;

/**
 * @author Carl Adler (C.A.)
 * */
public interface CrawlerTaskDao {
	
	public long queryIdByAPKFileName(String apkFileName);
	
	public List<APKManifest> findByAPKFileName(String apkFileName);
	
	public boolean checkVersionCodeEquality(String apkFileName, String versionCode);
	
	public int saveAPKMetaInfo(APKManifest apkManifest);
	
	public int saveAPKFingerPrintInfo(APKFingerPrintInfo apkfingerprint);
	
	public boolean updateAPKMetaInfo(APKManifest apkManifest);
	
	public boolean updateAPKFingerPrintInfo(APKFingerPrintInfo apkfingerprint);

}
