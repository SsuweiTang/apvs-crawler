package edu.ntust.apvs.crawler.dao.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.GenericXmlApplicationContext;

import edu.ntust.apvs.crawler.dao.CrawlerTaskDao;
import edu.ntust.apvs.crawler.domain.APKManifest;

public class CrawlerTaskDaoTest {

	GenericXmlApplicationContext ctx;
	CrawlerTaskDao crawlerDao;
	APKManifest apkManifest;
	APKManifest apkManifest2;
	
	@Before
	public void before() {
		ctx = new GenericXmlApplicationContext();
		ctx.load("classpath:app-context-test.xml");
		ctx.refresh();
		crawlerDao = ctx.getBean("crawlerTaskDao", CrawlerTaskDao.class);
		
		apkManifest = new APKManifest();
		apkManifest.setApkFileName("BMICalculator.apk");
		apkManifest.setShaForAndroidManifestXml("E5gjF+5Ch+aoz/IptekZvTb2oKs=");
		apkManifest.setShaForResourcesArsc("E5gjF+5Ch+aoz/IptekZvTb2oKs=");
		apkManifest.setShaForClassesDex("E5gjF+5Ch+aoz/IptekZvTb2oKs=");
		apkManifest.setMnftEntrySize(1245);
		apkManifest.setVersionCode("12345");
		
		apkManifest2 = new APKManifest();
		apkManifest2.setApkFileName("BMICalculator2.apk");
		apkManifest2.setShaForAndroidManifestXml("E5gjF+5Ch+aoz/IptekZvTb2oKs=");
		apkManifest2.setShaForResourcesArsc("E5gjF+5Ch+aoz/IptekZvTb2oKs=");
		apkManifest2.setShaForClassesDex("E5gjF+5Ch+aoz/IptekZvTb2oKs=");
		apkManifest2.setMnftEntrySize(1245);
		apkManifest2.setVersionCode("1234533");
	}
	
	@After
	public void after() {
		ctx.close();
	}
	
	@Test
	public void testForFindByAPKPackageName() {
		assertTrue(crawlerDao.findByAPKFileName(apkManifest.getApkFileName()) != null);
		assertTrue(crawlerDao.findByAPKFileName(apkManifest.getApkFileName()).size() > 0);
	}
	
	@Test
	public void testForCheckVersionCode() {
		assertTrue(crawlerDao.checkVersionCodeEquality(apkManifest.getApkFileName(), apkManifest.getVersionCode()));
		assertFalse(crawlerDao.checkVersionCodeEquality(apkManifest.getApkFileName(), apkManifest2.getVersionCode()));
	}
	
	@Test
	public void testForInsertAPKInfo() {
		assertEquals(1, crawlerDao.saveAPKMetaInfo(apkManifest2));
	}
	
	@Test
	public void testForUpdateVersionCode() {
		apkManifest = crawlerDao.findByAPKFileName(apkManifest.getApkFileName()).get(0);
		apkManifest.setVersionCode("98765");
		assertTrue(crawlerDao.updateAPKMetaInfo(apkManifest));
	}
	
	@Test
	public void testForNullAPKInfo() {
		int size = crawlerDao.findByAPKFileName("com.evernote.apk").size();
		assertEquals(0, size);
	}
	
}
