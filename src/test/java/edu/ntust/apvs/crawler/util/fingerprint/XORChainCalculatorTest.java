package edu.ntust.apvs.crawler.util.fingerprint;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.GenericXmlApplicationContext;

import edu.ntust.apvs.crawler.dao.CrawlerTaskDao;
import edu.ntust.apvs.crawler.domain.APKManifest;
import edu.ntust.apvs.crawler.domain.APKSubFileFingerPrint;
import edu.ntust.apvs.crawler.util.APVSConstantDescriptor;

public class XORChainCalculatorTest {
	
	List<APKSubFileFingerPrint> fingerPrintListForRes ;
	
	@Before
	public void before() {
		
		fingerPrintListForRes = new ArrayList<APKSubFileFingerPrint>();
		fingerPrintListForRes.add(ObjectTransformer.transformSubCategoryOfResToListAPKSubFileFingerPrint(APVSConstantDescriptor.RES_CATEGORY_XML, "4d195d51a7bb1907cec2380d1d4b5d53da0f6e78"));
		fingerPrintListForRes.add(ObjectTransformer.transformSubCategoryOfResToListAPKSubFileFingerPrint(APVSConstantDescriptor.RES_CATEGORY_PNG, "83b6100aa8c710e8091acc69992065ddde66c5c3"));

	}
	
	@After
	public void after() {
		
	}
	
	@Test
	public void testForCalculateXORSha1Value() {
	assertEquals(XORChainCalculator.calculateXORSha1Value(fingerPrintListForRes),"ceaf4d5b0f7c09efc7d8f464846b388e0469abbb");
	}

}
