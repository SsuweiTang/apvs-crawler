package edu.ntust.apvs.crawler.util.fingerprint;

import static org.junit.Assert.*;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.junit.Test;
import edu.ntust.apvs.crawler.domain.APKSubFileFingerPrint;

public class SHA1GeneratorTest {
	
	private static APKSubFileFingerPrint fileFingerPrint;
	private static InputStream is;

	@Test
	public void testForGenerateSha1ForFile() {
		
		try {
			is = new BufferedInputStream(new FileInputStream("D:\\GooglePlayCrawler_Home\\CrawlerSample\\com.evernote_oo\\AndroidManifest1.xml"));
		
		fileFingerPrint=SHA1Generator.generateSha1ForFile(is, "AndroidManifest1.xml");
		
		assertEquals("FpxWnAJoj/FosgQEOzKQ8VtdWrM=",fileFingerPrint.getSha1Base64());
		assertEquals("169c569c02688ff168b204043b3290f15b5d5ab3",fileFingerPrint.getSha1Hex());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Test(expected = FileNotFoundException.class)
	public void testForNoFile() throws FileNotFoundException
	{
		
		is = new BufferedInputStream(new FileInputStream(""));
		 fileFingerPrint=SHA1Generator.generateSha1ForFile(is, "AndroidManifest1.xml");
		assertEquals("FpxWnAJoj/FosgQEOzKQ8VtdWrM=",fileFingerPrint.getSha1Base64());
		assertEquals("169c569c02688ff168b204043b3290f15b5d5ab3",fileFingerPrint.getSha1Hex());
	}
	
	@Test
	public void testForGenerateSha1ForString() {
		
	String Sha1Hex=SHA1Generator.generateSha1ForString("a4d96ace643cf4be5264a35a92b58f98c4353c653418b2b39af16e4125e93afaf68a87baaf324e07");
	assertEquals("0572b2cedbc3ff009976db47cc059a4ffe22b45f",Sha1Hex);
	}
	

}
