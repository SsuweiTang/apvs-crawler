package edu.ntust.apvs.crawler.util.fingerprint;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;

import edu.ntust.apvs.crawler.util.APVSConstantDescriptor;
import edu.ntust.apvs.crawler.util.Order;
import edu.ntust.apvs.crawler.util.OrderedRunner;

@RunWith(OrderedRunner.class)
public class APKFingerPrintHanlderTest {

	@Order(order = 2)
	@Test
	public void testForMakeDir() {
		assertTrue(APKFingerPrintHandler.deleteDirectoryForCutFile(new File(APVSConstantDescriptor.APK_FILE_LOCATION2+"M10209101_HW4")));
		assertFalse(new File(APVSConstantDescriptor.APK_FILE_LOCATION2+"M10209101_HW4").exists());
	}
	
	@Order(order = 1)
	@Test
	public void testForCutFile() {
		String testFile=APVSConstantDescriptor.APK_FILE_LOCATION2+"M10209101_HW4\\AndroidManifest.xml";

		try {
			APKFingerPrintHandler.cutFile(new FileInputStream(testFile), APVSConstantDescriptor.APK_FILE_LOCATION2+"M10209101_HW4", "AndroidManifest", ".xml", 3);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		String cutFile1=APVSConstantDescriptor.APK_FILE_LOCATION2+"M10209101_HW4\\AndroidManifest1.xml";
		String cutFile2=APVSConstantDescriptor.APK_FILE_LOCATION2+"M10209101_HW4\\AndroidManifest2.xml";
		String cutFile3=APVSConstantDescriptor.APK_FILE_LOCATION2+"M10209101_HW4\\AndroidManifest3.xml";	
		assertTrue(new File(cutFile1).exists());
		assertTrue(new File(cutFile2).exists());
		assertTrue(new File(cutFile3).exists());
		
		
		
		try {
			FileInputStream fs1=new FileInputStream(cutFile1);
			FileInputStream fs2=new FileInputStream(cutFile2);
			FileInputStream fs3=new FileInputStream(cutFile3);
			
			assertEquals(fs1.available(),fs2.available());
			assertEquals(fs2.available(),fs3.available());
			assertEquals(fs1.available(),fs3.available());
		
			fs1.close();
			fs2.close();
			fs3.close();
		} catch (FileNotFoundException e) {	
			e.printStackTrace();
		} catch (IOException e) {	
			e.printStackTrace();
		}	
		
	}

}
