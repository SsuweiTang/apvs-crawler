package edu.ntust.apvs.crawler.service.impl;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.ntust.apvs.crawler.domain.APKFingerPrintInfo;
import edu.ntust.apvs.crawler.util.APVSConstantDescriptor;

/**
 * 
 * @author Carl Adler(C.A.)
 *
 */
public class APKFingerPrintServiceImplTest {
	
	@Test(timeout = 10000)
	public void test() {
		long start = System.currentTimeMillis();
		APKFingerPrintServiceImpl service = new APKFingerPrintServiceImpl();
		APKFingerPrintInfo aPKFingerPrintInfo = service.generateAPKFingerPrintFromAPKFile(APVSConstantDescriptor.APK_FILE_LOCATION2 + "M10209101_HW4.apk");
//		assertEquals("a4d96ace643cf4be5264a35a92b58f98c4353c65", aPKFingerPrintInfo.getFingerPrintForClassesDex());
//		assertEquals("3418b2b39af16e4125e93afaf68a87baaf324e07", aPKFingerPrintInfo.getFingerPrintForResourcesArsc());
//		assertEquals("f80dcee8a310a0c9e9c22d02e118a0db831baca4", aPKFingerPrintInfo.getFingerPrintForAndroidManifestXml());
//		assertEquals("ee68dc494437691422c922b707899cb599998894", aPKFingerPrintInfo.getFingerPrintForResXml());
//		assertEquals("e1ff450befd5aa69eb174ee39a089e726cf0647e", aPKFingerPrintInfo.getFingerPrintForResImg());
//		assertEquals("f979942abe2c37dc9de6c549d8102c7f569ecea", aPKFingerPrintInfo.getFingerPrintForRes());
//		assertEquals("4157581083114554639d1a2681d9d20a58539a75", aPKFingerPrintInfo.getFingerPrintForEntireAPKFile());
		assertNotNull(aPKFingerPrintInfo);
		System.out.println(aPKFingerPrintInfo.getFingerPrintForEntireAPKFile());
		long end = System.currentTimeMillis();
		long cost = (end - start) ;
	
//		System.out.printf("Cost: %d", cost);
		System.out.println(cost);
		System.out.printf("Cost: %6.3fs", cost);
	}

}
