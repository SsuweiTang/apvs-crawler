package edu.ntust.apvs.crawler.service.impl;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import edu.ntust.apvs.crawler.domain.APKFingerPrintInfo;
import edu.ntust.apvs.crawler.util.APVSConstantDescriptor;

public class APKFingerPrintServiceImplTest2 {

	@Test(timeout = 100000)
	public void test() {
		
		File fileFolder=new File("D:\\apk\\21\\c");
		File[] files = fileFolder.listFiles();
		if (files != null) {
			for (File f : files) {
				
				if (f.isFile() && f.exists()) {
						
					System.out.println(f.getName()+"  ");	
					
					
					if(f.getName().endsWith(".apk"))
					{
					long start = System.currentTimeMillis();
					APKFingerPrintServiceImpl service = new APKFingerPrintServiceImpl();
					APKFingerPrintInfo aPKFingerPrintInfo = service.generateAPKFingerPrintFromAPKFile(APVSConstantDescriptor.APK_FILE_LOCATION2 +"21\\c\\"+ f.getName());
					assertNotNull(aPKFingerPrintInfo);
					System.out.println(aPKFingerPrintInfo.getFingerPrintForEntireAPKFile());
					long end = System.currentTimeMillis();
					float cost = (end - start) ;
//					System.out.println("Cost:"+ cost);
					cost = cost / 1000;
					System.out.printf("Cost: %6.3fs", cost);
					System.out.println();
					}
				
					
			}
				
		}
		}
		
//		long start = System.currentTimeMillis();
//		APKFingerPrintServiceImpl service = new APKFingerPrintServiceImpl();
//		APKFingerPrintInfo aPKFingerPrintInfo = service.generateAPKFingerPrintFromAPKFile(APVSConstantDescriptor.APK_FILE_LOCATION2 +"19\\"+ "com.mxtech.ffmpeg.tegra3.apk");
//
//		assertNotNull(aPKFingerPrintInfo);
//		System.out.println(aPKFingerPrintInfo.getFingerPrintForEntireAPKFile());
//		long end = System.currentTimeMillis();
//		long cost = (end - start) ;
//		System.out.println("Cost:"+ cost);
	}

}
